<?php


include_once 'init.php';

$id = $_GET["id"];

$sql = "SELECT * FROM `all_products` WHERE `id`= $id" ;

$result = mysqli_query($con,$sql);
$response = array();

while($row = mysqli_fetch_array($result))

{
 array_push($response,array(
     "id"=>$row[0],
     "name"=>$row[1],
     "product_type"=>$row[2],
     "subCategory_id"=>$row[3],
     "restaurant_id"=>$row[4],
     "image"=>$row[5],
     "description"=>$row[6],
     "food_type"=>$row[7],
     "size"=>$row[8],
     "medicine_mg_ml"=>$row[9],
     "medicine_company"=>$row[10],
     "purchasePrice"=>$row[11],
     "price"=>$row[12],
     "discount_price"=>$row[13],
     "discount_percent"=>$row[14],
     "opening_time"=>$row[15],
     "closing_time"=>$row[16],
     "availability"=>$row[17]
    ));
}

echo json_encode($response);

mysqli_close($con);

?>